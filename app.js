/* jshint node: true */
/*eslint no-console: "error"*/
'use strict';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var config = require('./config');

var app = express();

require('./config/express')(app);
require('./config/mongoose');

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  app.use('/', express.static('public'));
  app.use('/', express.static('public/app'));
  app.use('/', express.static('public/docs'));
} else if (process.env.NODE_ENV === 'production') {
  app.use('/', express.static('public/dist'));
} else {
  app.use('/', express.static('public/dist'));
  app.use('/documents/api', express.static('docs/api-doc'));
}

require('./config/error-messages.js')(app);

module.exports = app;
